require 'test_helper'

class ItemTest < ActiveSupport::TestCase
  def setup
    @drawer = drawers(:default)
    @item = @drawer.items.build(title: 'plain tee')
  end

  test 'item should be valid' do
    assert @item.valid?
  end

  test 'title should be present' do
    @item.title = '  '
    assert_not @item.valid?
  end

  test 'title should not be too long' do
    @item.title = 'j' * 51
    assert_not @item.valid?
  end

  test 'drawer_id exists' do
    @item.drawer_id = nil
    assert_not @item.valid?
  end

  test 'dirty exists' do
    @item.dirty = nil
    assert_not @item.valid?
  end

  test 'item should be displayed with the most recent item first' do
    assert_equal items(:most_recent), Item.first
  end
end
