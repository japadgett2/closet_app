require 'test_helper'

class DrawerTest < ActiveSupport::TestCase
  def setup
    @user = users(:sterling)
    @drawer = @user.drawers.build(clothing: 'Shirts')
  end

  test 'should be valid' do
    assert @drawer.valid?
  end

  test 'user id should be present' do
    @drawer.user_id = nil
    assert_not @drawer.valid?
  end

  test 'clothing should be present' do
    @drawer.clothing = '   '
    assert_not @drawer.valid?
  end

  test 'clothing should be no more than 50 characters' do
    @drawer.clothing = 'j' * 51
    assert_not @drawer.valid?
  end

  test 'associated items should be destroyed if drawer is destroyed' do
    @drawer.save
    @drawer.items.create!(title: 'Bars')
    assert_difference 'Item.count', -1 do
      @drawer.destroy
    end
  end
end
