require 'test_helper'

class EditItemTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:sterling)
    @drawer = drawers(:default)
    @item = items(:black_shirt)
  end

  test 'should change the :dirty attribute from false to true' do
    log_in_as(@user)
    get user_items_today_path(@user)
    assert_equal false, @item.dirty
    patch user_drawer_item_path(@user, @drawer, @item), params: { item:
      { dirty: true } }
    @item.reload

    assert_equal true, @item.dirty
    assert_redirected_to user_items_today_path(@user)
  end

  test 'should change the :dirty attribute from true to false' do
    log_in_as(@user)
    get user_items_today_path(@user)
    @item.update_attribute(:dirty, true)
    @item.reload
    assert_equal true, @item.dirty
    patch user_drawer_item_path(@user, @drawer, @item), params: { item:
      { dirty: false } }
    @item.reload

    assert_equal false, @item.dirty
    assert_redirected_to user_items_hamper_path(@user)
  end
end
