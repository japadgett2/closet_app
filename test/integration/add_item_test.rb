require 'test_helper'

class AddItemTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:sterling)
    @drawer = drawers(:default)
    @item = items(:black_shirt)
  end

  test 'should create item' do
    log_in_as(@user)
    get user_drawer_path(@user, @drawer)
    assert_difference 'Item.count', 1 do
      post user_drawer_items_path(@user, @drawer),
           params: { item: { title: @item.title } }
    end
    assert_redirected_to user_drawer_url(@user, @drawer)
    follow_redirect!
    assert_not flash.empty?
  end
end
