require 'test_helper'

class DeleteItemTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:sterling)
    @drawer = drawers(:default)
    @item = items(:black_shirt)
  end

  test 'should delete item' do
    log_in_as(@user)
    get user_drawer_path(@user, @drawer)
    assert_difference 'Item.count', -1 do
      delete user_drawer_item_path(@user, @drawer, @item)
    end
    assert_redirected_to user_drawer_path(@user, @drawer)
    follow_redirect!
    assert_not flash.empty?
  end
end
