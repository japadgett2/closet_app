require 'test_helper'

class FinishedLaundryTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:sterling)
    @drawer = drawers(:default)
    @item = items(:black_shirt)
  end

  test 'Finished Laundry button should change the :dirty attibute from true to
        false for all items' do
    log_in_as(@user)
    get user_items_hamper_path(@user)
    @item.update_attribute(:dirty, true)
    @item.reload
    assert_equal 1, total_dirty(@drawer)
    patch user_drawer_clean_path(@user, @drawer)
    @item.reload
    @drawer.reload
    assert_equal 0, total_dirty(@drawer)
  end
end
