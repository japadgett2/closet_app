require "application_system_test_case"

class UsersTest < ApplicationSystemTestCase
  test 'creating a new User' do
    # Visit the home page
    visit root_url
    
    # Click the Sign up Now! button
    click_button 'Sign up Now!'
    assert_selector 'h1', text: 'Sign up'

    # Submit the form
    fill_in 'Name', with: 'Bill Nye'
    fill_in 'Email', with: 'bill@example.com'
    fill_in 'Password', with: 'science'
    fill_in 'Password confirmation', with: 'science'
    click_button 'Create my account'

    # Verify that the User was created
    assert_selector 'h1', text: 'My Clean Clothes'
    assert_selector '.alert', text: 'Welcome to the Closet Ordering App!'
  end
end
