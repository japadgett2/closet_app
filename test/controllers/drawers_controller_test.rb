require 'test_helper'

class DrawersControllerTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:sterling)
    @drawer = drawers(:default)
  end

  test 'should show My Drawer' do
    log_in_as(@user)
    get user_drawer_path(@user, @drawer)
    assert_response :success
    assert_select 'title', 'My Drawer | Dresser Ordering App'
  end

  test 'should redirect all_clean when not logged in' do
    patch user_drawer_clean_path(@user, @drawer)
    assert_not flash.empty?
    assert_redirected_to login_url
  end
end
