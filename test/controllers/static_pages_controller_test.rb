require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest
  def setup
    @base_title = 'Dresser Ordering App'
  end

  test 'get root is home' do
    get root_path
    assert_response :success
    assert_select 'h1', 'Welcome to the dresser ordering app!'
  end

  test 'should get home' do
    get root_path
    assert_response :success
    assert_select 'title', @base_title
  end

  test 'should get help' do
    get features_path
    assert_response :success
    assert_select 'title', "Upcoming Features | #{@base_title}"
  end

  test 'should get about' do
    get about_path
    assert_response :success
    assert_select 'title', "About | #{@base_title}"
  end

  test 'should get contact' do
    get contact_path
    assert_response :success
    assert_select 'title', "Contact | #{@base_title}"
  end
end
