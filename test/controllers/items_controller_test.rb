require 'test_helper'

class ItemsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:sterling)
    @drawer = drawers(:default)
    @item = items(:black_shirt)
  end

  test 'should show hamper' do
    log_in_as(@user)
    get user_items_hamper_path(@user)
    assert_response :success
    assert_select 'title', 'My Hamper | Dresser Ordering App'
  end

  test 'should show today' do
    log_in_as(@user)
    get user_items_today_path(@user)
    assert_response :success
    assert_select 'title', 'Today | Dresser Ordering App'
  end

  test 'should redirect hamper when not logged in' do
    get user_items_hamper_path(@user)
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test 'should redirect today when not logged in' do
    get user_items_today_path(@user)
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test 'should redirect update when not logged in' do
    patch user_drawer_item_path(@user, @drawer, @item), params: { item:
      { dirty: true } }
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test 'should redirect destroy when not logged in' do
    delete user_drawer_item_path(@user, @drawer, @item)
    assert_not flash.empty?
    assert_redirected_to login_url
  end
end
