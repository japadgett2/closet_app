# Dresser Order Application

APPLICATION UNDER CONSTRUCTION

This application was built to help order your dresser.  If you are like me, you
use a LIFO ordering scheme in your dresser.  It's easy to keep this order as
long as you don't wash your shirts or wash them every day.  But if you let a few
days go by, then keeping track of the order becomes a task.  Enter the dresser
ordering app.  This project is being built for fun, it won't save you time, use
at your own risk.

## Getting started

To get started with the app, clone the repo and then install the needed gems:

```
$ bundle install --without production
```

Next, migrate the database:

```
$ rails db:migrate
```

Finally, run the test suite to verify that everything is working correctly:

```
$ rails test
```

If the test suite passes, you'll be ready to run the app in a local server:

```
$ rails server
```

## Built With
* Ruby on Rails

## License

This project is licensed underder the MIT License - see the LICENSE.md file for 
details
 
## Acknowledgments
* Michael Hartl Rails Tutorial for helping me feel comfortable attempting my
own rails app
