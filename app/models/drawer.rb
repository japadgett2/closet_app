class Drawer < ApplicationRecord
  belongs_to :user
  has_many :items, dependent: :destroy
  validates :user, presence: true
  validates :user_id, presence: true
  validates :clothing, presence: true, length: { maximum: 50 }
end
