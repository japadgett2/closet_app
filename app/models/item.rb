class Item < ApplicationRecord
  belongs_to :drawer
  default_scope -> { order(updated_at: :desc) }
  validates :drawer, presence: true
  validates :title, presence: true, length: { maximum: 50 }
  validates :drawer_id, presence: true
  validates :dirty, inclusion: { in: [true, false] }
end
