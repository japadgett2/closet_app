module DrawersHelper
  # finds the drawer associated with the current user
  # will be invalid when multiple drawer functionality is added
  def current_drawer
    @current_drawer ||= Drawer.find_by(user_id: session[:user_id])
  end
end
