module ApplicationHelper
  # optionally takes a provided page title and appends a suffix string
  def full_title(page_title = '')
    base_title = 'Dresser Ordering App'
    page_title.empty? ? base_title : page_title + ' | ' + base_title
  end
end
