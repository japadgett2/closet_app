class DrawersController < ApplicationController
  before_action :logged_in_user
  before_action :set_correct_user

  # shows a drawer (one for now, more to come in version 1.1)
  def show
    @drawer = @user.drawers.find(params[:id])
    @items = @drawer.items
  end

  # changes the :dirty column value in all items in a drawer from true to false
  def all_clean
    @drawer = @user.drawers.find(params[:drawer_id])
    @drawer.items.reverse_each do |item|
      item.update_attribute(:dirty, false)
    end
    redirect_to user_items_hamper_path
    flash[:success] = 'Items returned to drawer'
  end
end
