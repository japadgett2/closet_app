class ItemsController < ApplicationController
  before_action :logged_in_user
  before_action :set_correct_user
  before_action :set_drawer, except: [:today, :hamper]
  before_action :set_all_drawers, only: [:today, :hamper]
  before_action :set_all_items, only: [:today, :hamper]

  # creates an item
  def create
    @item = @drawer.items.new(item_title_param)

    if @item.save
      redirect_to user_drawer_path(@user, @drawer)
      flash[:success] = 'Item Added to My Drawer'
    else
      render 'drawers/show'
    end
  end

  # shows a list of all clean items
  def today; end

  # shows a list of all dirty items
  def hamper; end

  # changes the :dirty attribute from false to true for an item
  def update
    @item = @drawer.items.find(params[:id])
    @item.update_attributes(item_dirty_param)
    if @item.dirty
      flash[:success] = 'Item moved to hamper'
      redirect_to user_items_today_path(@user)
    else
      flash[:success] = 'Item returned to drawer'
      redirect_to user_items_hamper_path
    end
  end

  # destroys an Item
  def destroy
    Item.find(params[:id]).destroy
    flash[:success] = 'Item deleted'
    redirect_to user_drawer_path(@user, @drawer)
  end

  private

    # retrieves :title value from params
    def item_title_param
      params.require(:item).permit(:title)
    end

    # retrieves :dirty value from params
    def item_dirty_param
      params.require(:item).permit(:dirty)
    end

    # finds a drawer from params
    def set_drawer
      @drawer = @user.drawers.find(params[:drawer_id])
    end

    # finds all drawers of a user
    def set_all_drawers
      @drawers = @user.drawers
    end

    # finds all items of a user
    def set_all_items
      @items = @user.items
    end
end
