class UsersController < ApplicationController
  before_action :logged_in_user, only: [:edit, :update, :index, :destroy]
  before_action :correct_user,   only: [:edit, :update]
  before_action :admin_user,     only: [:index]

  # displays a list of all users
  def index
    @users = User.paginate(page: params[:page])
  end

  # displays a particular user
  def show
    @user = User.find(params[:id])
  end

  # displays a signup page
  def new
    @user = User.new
  end

  # creates a User
  def create
    @user = User.new(user_params)

    if @user.save
      # this is named 'My Drawer bc at some point adding drawers will be an
      # option, in which case you could have a drawer for 'shirts', 'pants', etc
      @user.drawers.create!(clothing: 'My Drawer')
      log_in @user
      flash[:success] = 'Welcome to the Closet Ordering App'
      redirect_to user_items_today_path(@user)
    else
      render 'new'
    end
  end

  # displays a form to edit login credentials
  def edit; end

  # updates login credentials
  def update
    if @user.update_attributes(user_params)
      flash[:success] = 'Profile updated'
      redirect_to @user
    else
      render 'edit'
    end
  end

  # destroys a User
  def destroy
    @user = User.find(params[:id])
    if current_user?(@user) || current_user.admin?
      User.find(params[:id]).destroy
      flash[:success] = 'User deleted'
      current_user.admin? ? (redirect_to users_url) : (redirect_to root_url)
    else
      redirect_to root_url
    end
  end

  private

    # Retrieves signup credentials from params hash
    def user_params
      params.require(:user).permit(:name, :email, :password,
                                   :password_confirmation)
    end

    # Confirms an admin user.
    def admin_user
      redirect_to(root_url) unless current_user.admin?
    end

    # Confirms the correct user with params value of :id
    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end
end
