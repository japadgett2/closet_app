class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include SessionsHelper
  # include ItemsHelper

  private

  # verifies that user is logged in, redirects to login page if not.
  def logged_in_user
    unless logged_in?
      store_location
      flash[:danger] = 'Please log in.'
      redirect_to login_url
    end
  end

  # Confirms correct user with params value of :user_id
  def set_correct_user
    @user = User.find(params[:user_id])
    redirect_to(root_url) unless current_user?(@user)
  end
end
