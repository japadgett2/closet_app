class StaticPagesController < ApplicationController
  def home; end

  def features; end

  def about; end

  def contact; end
end
