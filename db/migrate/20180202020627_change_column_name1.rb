class ChangeColumnName1 < ActiveRecord::Migration[5.1]
  def change
    rename_column :drawers, :type, :clothing
  end
end
