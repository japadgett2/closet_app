class ChangeColumnName < ActiveRecord::Migration[5.1]
  def change
    rename_column :drawers, :content, :type
  end
end
