Rails.application.routes.draw do
  root   'static_pages#home'
  get    '/features',    to: 'static_pages#features'
  get    '/about',       to: 'static_pages#about'
  get    '/contact',     to: 'static_pages#contact'
  get    '/signup',      to: 'users#new'
  post   '/signup',      to: 'users#create'
  get    '/login',       to: 'sessions#new'
  post   '/login',       to: 'sessions#create'
  delete '/logout',      to: 'sessions#destroy'

  resources :users do
    get '/today',        to: 'items#today',       as: 'items_today'
    get '/hamper',       to: 'items#hamper',     as: 'items_hamper'
    resources :drawers, only: :show do
      patch '/dirty',    to: 'drawers#all_clean', as: 'clean'
      resources :items, only: [:create, :update, :destroy]
    end
  end
end
